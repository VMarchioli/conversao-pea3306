%NUSP utilizado = 10773075
%Parametros nominais do transformador
S_nom = 2e3;
S_nom = 0.3*S_nom:1:1.3*S_nom;
f = 60;
V_alta = 230;
V_baixa = 115;
a = 230/115;
fp = 0.7; %indutivo
P = fp*S_nom;
Q = sin(acos(fp))*S_nom;

%Parametros referidos ao lado de alta tensao
Zcc = 0.56 + 1i*2.13;
r1_alta = 0.56/2;
r2_alta = 0.56/2; %refletido na alta
Xd1_alta = 2.13/2;
Xd2_alta = 2.13/2; %refletido na alta
Ym_alta = (1/1763 - 1i*(1/564));
Zm_alta = 1/Ym_alta;
rp_alta = 1763;
Xm_alta = 564;
Zcarga = (V_alta^2)./conj(P + 1i*Q);
Icarga = V_alta./Zcarga;
I2 = Icarga;

%Parametros referidos ao lado de baixa tensao
r1_baixa = r1_alta/a^2;
r2_baixa = r2_alta/a^2;
Xd1_baixa = Xd1_alta/a^2;
Xd2_baixa = Xd2_alta/a^2;
rp_baixa = rp_alta/a^2;
Xm_baixa = Xm_alta/a^2;
Zm_baixa = Zm_alta/a^2;
Zcarga_linha = Zcarga/a^2;
I2_l = I2*a;

%Calculo das correntes
Vm = I2_l.*(r2_baixa + 1i.*Xd2_baixa) + V_alta/a;
I0 = Vm/Zm_baixa;
I1 = I0 + I2_l;

%Tensao de entrada
Vin = I1.*(r1_baixa + 1i.*Xd1_baixa) + Vm;

%Tensao de entrada para fluxo constante
Vin_2 = I2_l.*(r1_baixa + 1i.*Xd1_baixa + r2_baixa + 1i.*Xd2_baixa) + V_alta/a;

%Regulacao
%Vfi = Vin*(Zm_baixa)./(Zm_baixa + r1_baixa + 1i.*Xd1_baixa);
%R = ((abs(Vfi) - abs(V_baixa))./abs(V_baixa))*100;
R = ((abs(Vin) - abs(V_baixa))./abs(V_baixa))*100;
%Vfi eh Vmag que cai sem carga e com fonte igual a Vin calculado
Rfluxocte = ((abs(Vin_2) - V_baixa)./abs(V_baixa))*100;

%Rendimento
Pfi = (abs(Vm).^2)./(rp_baixa);
Penr = (abs(I1).^2)*r1_baixa + (abs(I2_l).^2)*r2_baixa;
Pperdas = Pfi + Penr; 
eta = (P./(P + Pperdas))*100;

Pfi_cte = (abs(Vin_2).^2)/(rp_baixa);
Penr_cte = 2*(abs(I2_l).^2)*r1_baixa;
Pperdas_cte = Pfi_cte + Penr_cte;
etafluxocte = (P./(P + Pperdas_cte))*100;

%Encontrar ponto de plena carga
k = find(S_nom == 2000);
Rnom = R(k);
Rnomcte = Rfluxocte(k);
etanom = eta(k);
etanomcte = etafluxocte(k);
S = S_nom*100/2000;

%Graficos
figure(1)
hold on
plot(S, R, 'r');
plot(100, Rnom, 'ro'); %100 por cento
plot(S, Rfluxocte, 'b');
plot(100, Rnomcte, 'b-s');

title('Regula��o')
xlabel('S (%)')
ylabel('Regula��o (%)')
legend('Regula��o', 'Ponto de plena carga', 'Regula��o com fluxo constante', 'Ponto de plena carga')
grid on
hold off

figure(2)
hold on
plot(S, eta, 'r');
plot(100, etanom, 'ro');
plot(S, etafluxocte, 'b');
plot(100, etanomcte, 'b-s');
title('Rendimento')
xlabel('S (%)')
ylabel('Rendimento (%)')
legend('Rendimento', 'Ponto de plena carga', 'Rendimento com fluxo constante', 'Ponto de plena carga')
grid on
hold off


