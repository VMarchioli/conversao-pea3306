clc;
clear;

entreferro_vertical = [1e-3 2e-3 3e-3 4e-3 5e-3];
N = 600;
I = 1;
                                                               
R_vertical = (entreferro_vertical)./(4*pi*1e-7*pi*(25e-3).^2);
R_horizontal = log(2.65/2.5)/(4*pi*1e-7*(2*pi*25e-3));
Req = R_vertical + R_horizontal;
L_analitico = (N^2)./Req;
L_1000 = [0.643541, 0.447887, 0.357926, 0.306488, 0.272859];
L_steel = [0.682736, 0.464717, 0.367756, 0.313211, 0.277896];
plot(L_analitico);
hold;
plot(L_1000);
plot(L_steel);
legend('Anal�tico','Mu\_1000', 'M-15 steel')
figure(1);
grid;
title("Indut�ncia pr�pria da bobina em fun��o do entreferro inferior");
xlabel("Entreferro inferior [mm]");
ylabel("Indut�ncia pr�pria [H]")


    
%------------FOR�A--------------%

figure(3);
F_analitico = -(N*N*I*I)./(2*Req.^2*(4*pi*1e-7*pi*(25e-3).^2));
F_1000 = [-156.904, -61.4289, -32.8157, -20.4341, -13.9607];
F_steel = [-177.95, -66.5853, -34.8597, -21.4643, -14.5597];
plot(F_analitico);
hold;
plot(F_1000);
plot(F_steel)
legend('Anal�tico','Mu\_1000', 'M-15 steel')
grid;
title("For�a no �mbolo em fun��o do entreferro inferior");
xlabel("Entreferro inferior [mm]");
ylabel("For�a [N]")


%-------------Erros-------------%

Erro_L1000 = (L_1000-L_analitico)*100./L_analitico;
Erro_Lsteel = (L_steel-L_analitico)*100./L_analitico;

Erro_F21000 = (F_1000-F_analitico)*100./F_analitico;
Erro_F2steel = (F_steel-F_analitico)*100./F_analitico;